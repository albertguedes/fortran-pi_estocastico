    PROGRAM pi_estocastico

    use library

    implicit none

    ! n amostras
    ! m series
    integer m,n
    parameter(m=10)
    parameter(n=1000)

    integer i,j

    ! p é o ponto qualquer do quadrado.
    ! p0 é o centro do círculo de raio 1.
    complex p, p0

    ! f é a frequencia dos pontos que caem no círculo.
    ! media é a média das amostras
    ! erro é o erro da amostragem.
    real f, media, erro

    media = 0.0
    erro = 0.0

    ! Centro do círculo de raio 1.
    p0 = ( 1.0 , 1.0 )

    do i=1,m

        f = 0
        do j=1,n

            call random_complex(p)
            p = 2*p ! O quadrado tem área igual ao diâmetro do círculo d = 2r = 2

            ! Verifica se o ponto caiu dentro do circulo e conta.
            if( abs( p - p0 ) < 1 )then
                f = f + 1
            endif

        enddo
        f = f/n ! Calcula a fração de pontos dentro do círculo

        ! Coleta inicial para calculo de media e erro.
        media = media + f
        erro  = erro  + f**2

    enddo

    ! <f> = ( sum f_i ) / m
    media = media/m

    ! sig^2(f) = <f^2> - <f>^2
    erro = sqrt( erro/m - media**2 )

    write(*,"(F10.8,A,F10.8)"), 4*media," +/- ", 4*erro

    END PROGRAM pi_estocastico
